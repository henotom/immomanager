import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';

export const Floors = new Mongo.Collection('floors');

Meteor.methods({
    'floors.insert'(img) {
        return Floors.insert({
            image: img,
            createdAt: new Date(),
        });
    },

    'floors.removeByImg'(img, propertyId) {

        return Floors.remove({img: { $_id : img._id}},
            function (error, result) {
            }
        );

    },
});