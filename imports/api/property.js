import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

export const Properties = new Mongo.Collection('properties');

Meteor.methods({
    'properties.insertNew'() {

        return Properties.insert({
            title: '',
            createdAt: new Date(),
        });
    },

    'properties.addFloor'(propertyId, floor){
        Properties.update(propertyId,
            {
                $push: {
                    floors: floor

                }
            });
    },

    'properties.removeFloor'(propertyId, floor){
        Properties.update(propertyId,
            {
                $pull: {
                    floors: floor

                }
            });
    },

    'properties.remove'(propertyId) {
        Properties.remove(propertyId);
    },

    'properties.updateFirstStep'(propertyId, title, type, surface, city, address) {

        Properties.update(propertyId,
            {
                $set: {
                    title: title,
                    surface: surface,
                    type: type,
                    city: city,
                    address: address
                }
            });
    },
});