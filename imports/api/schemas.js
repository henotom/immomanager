PropertySchema = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200
    },

    type: {
        type: String,
        label: "Type"
    },

    surface: {
        type: Number,
        label: "Surface",
        decimal: true,
    },

    city: {
        type: String,
        label: "City"
    },

    address: {
        type: String,
        label: "Type"
    },

    floors: {
        type: [FloorSchema],
        label: "Floor"
    }
});


FloorSchema = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
    },

    uri: {
        type: String,
        label: "URI"
    },

    rooms: {
        type: [RoomSchema],
        label: "Rooms"
    }
});

RoomSchema = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
    },

    uri: {
        type: String,
        label: "URI"
    },

    noFloatingMap: {
        type: Boolean,
        label: "No Floating Map",
        optional: true,
    },

    dimensions: {
        type: Number,
        label: "Dimensions",
        optional: true,
        decimal: true,
    },

    tooltips: {
        type: [TooltipSchema],
        label: "Tooltips",
    }
});

TooltipSchema = new SimpleSchema({
    text: {
        type: String,
        label: "Text",
        optional: true,
    },

    title: {
        type: String,
        label: "Title",
        optional: true,
    },

    position: {
        type: [Number],
        label: "Position",
        decimal: true,
    },

    type: {
        type: String,
        label: "Type",
        optional: true,
    },

    width: {
        type: Number,
        label: "Width",
        optional: true,
        decimal: true,
    },

    height: {
        type: Number,
        label: "Height",
        optional: true,
        decimal: true,
    },

    source: {
        type: String,
        label: "Source",
        optional: true,
    },

    linkedRoomId: {
        type: String,
        label: "Linked Room Id",
        optional: true,
    },

    landscape: {
        type: Boolean,
        label: "Landscape",
        optional: true,
    }

});