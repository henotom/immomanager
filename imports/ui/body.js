import {Template} from 'meteor/templating';


import './steps/steps.js'
import './body.html';

Template.body.events({
    'click .backButton' (event){
        $('#carousel').slickPrev();
    },

    'click .nextButton' (event){
        $('#carousel').slickNext();
    },

    'focus input' (event){
        $(event.target).closest('.card').addClass('focused');
    },

    'blur input' (event){
        $(event.target).closest('.card').removeClass('focused');
    }
});