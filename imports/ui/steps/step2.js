import './steps.js';
import './step2.html';

import {Session} from 'meteor/session'
import {Floors} from '../../api/floor.js';
import {Properties} from '../../api/property.js';

var myData;

myData = FileCollection({
    resumable: true,
    resumableIndexName: 'test',
    http: [             // Define HTTP route
        {
            method: 'get',  // Enable a GET endpoint
            path: '/:uploadDate',  // this will be at route "/gridfs/myFiles/:md5"
            lookup: function (params, query) {  // uses express style url params
                return {filename: params.uploadDate};       // a query mapping url to myFiles
            }
        }]
});


if (Meteor.isClient) {
    Template.body.onRendered(function () {
        $('.fileDrop').bind('dragenter', handleDragOver);
        $('.fileDrop').bind('dragleave', handleDragLeave);
        myData.resumable.assignDrop($('.fileDrop'));
    });

    Meteor.startup(function () {
        myData.resumable.on('fileAdded', function (file) {
            Session.set(file.uniqueIdentifier, 0);
            return myData.insert({
                _id: file.uniqueIdentifier,
                filename: file.fileName,
                contentType: file.file.type
            }, function (err, _id) {
                if (err) {
                    console.warn("File creation failed!", err);
                    return;
                }

                Meteor.call('floors.insert',
                    myData.findOne(_id),

                    function (error, result) {
                        Meteor.call('properties.addFloor', Session.get('propertyActive'), {
                            floor: Floors.findOne(result)
                        });
                    });


                return myData.resumable.upload();
            });
        });
        myData.resumable.on('fileProgress', function (file) {
            return Session.set(file.uniqueIdentifier, Math.floor(100 * file.progress()));
        });
        myData.resumable.on('fileSuccess', function (file) {
            return Session.set(file.uniqueIdentifier, void 0);
        });
        return myData.resumable.on('fileError', function (file) {
            console.warn("Error uploading", file.uniqueIdentifier);
            return Session.set(file.uniqueIdentifier, void 0);
        });
    });

    Tracker.autorun(function () {
        var userId;
        userId = Meteor.userId();
        Meteor.subscribe('allData', userId);
        return $.cookie('X-Auth-Token', Accounts._storedLoginToken(), {
            path: '/'
        });
    });

    Template.plansupload.events({
        /*        'click .del-file': function (e, t) {
         if (Session.get("" + this._id)) {
         console.warn("Cancelling active upload to remove file! " + this._id);
         myData.resumable.removeFile(myData.resumable.getFromUniqueIdentifier("" + this._id));
         }

         Meteor.call('floors.removeByImg',
         myData.find(this.id), Session.get('propertyActive'));

         return myData.remove({
         _id: this._id
         });
         },*/
    });

    Template.plansupload.helpers({
        dataEntries: function () {
//            console.log(Properties.findOne({}, {sort: {createdAt: -1, limit: 1}}));
            let images = [];
            if (Properties.findOne({}, {
                    sort: {
                        createdAt: -1,
                        limit: 1
                    }
                }) !== undefined && Properties.findOne({}, {sort: {createdAt: -1, limit: 1}}).floors !== undefined) {
                (Properties.findOne({}, {sort: {createdAt: -1, limit: 1}}).floors).forEach(function (floor) {
                    images.push((floor['floor']['image']));
                });

                return images;
            }
        },

        uploadProgress: function () {
            var percent;
            return percent = Session.get("" + this._id);
        },

        isImage: function () {
            var types;
            types = {
                'image/jpeg': true,
                'image/png': true,
                'image/gif': true,
                'image/tiff': true
            };
            return (types[this.contentType] != null) && this.md5 !== 'd41d8cd98f00b204e9800998ecf8427e';
        },

        id: function () {
            return "" + this._id;
        },

        link: function () {
            return myData.baseURL + '/'+ this.uploadDate;
        },

        uploadStatus: function () {
            var percent;
            percent = Session.get("" + this._id);
            if (percent == null) {
                return "Processing...";
            } else {
                return "Uploading...";
            }
        },

        loginToken: function () {
            Meteor.userId();
            return Accounts._storedLoginToken();
        },
        userId: function () {
            return Meteor.userId();
        }

    });

    var handleDragOver = function (e) {
        $(this).addClass('dropzone--selected');
        return false;
    };

    var handleDragLeave = function (e) {
        $(this).removeClass('dropzone--selected');
        return false;
    };
}


