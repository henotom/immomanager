import {Template} from 'meteor/templating';
import {Meteor} from 'meteor/meteor';
import {Session} from 'meteor/session'

import './step0.js';
import './step1.js';
import './step2.js';
import './steps.html';
import '../body.html';

import {Floors} from '../../api/floor.js';
import {Properties} from '../../api/property.js';
import {Rooms} from '../../api/room.js';
import {Tooltips} from '../../api/tooltip.js';

Template.body.onCreated(function bodyOnCreated() {
    Meteor.subscribe('properties');
    Meteor.subscribe('floors');
    Meteor.subscribe('allData');

});

Template.body.helpers({
    properties() {
        return Properties.find({});
    },

    rooms() {
        return Rooms.find({});
    },

    tooltips() {
        return Tooltips.find({});
    },

    floors() {
        return Floors.find({});
    },
});

Template.steps.rendered = function () {
    $('#carousel').slick({
        dots: false,
        arrows: false,
        swipe: false,
        slidesToShow: 1,
        infinite: false,
        centerMode: true,
    });
};

Template.body.events({
    'click #add'(event, instance){
        Meteor.call('properties.insertNew', function (error, result) {
            Session.set('propertyActive', Properties.findOne({_id: result}));
        });
    },

    'click #step1'(event, instance){
        let title = $('#title').val();
        let surface = $('#surface').val();
        let city = $('#city').val();
        let address = $('#address').val();
        let type = $("#type option:selected").text();

        Meteor.call('properties.updateFirstStep',
            Session.get('propertyActive'),
            title, type, surface, city, address);
    },

    'click #step2'(event){

    },


});


