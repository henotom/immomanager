import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

import {Properties} from '../imports/api/property.js';
import {Floors} from '../imports/api/floor.js';

var myData;

myData = FileCollection({
    resumable: true,
    resumableIndexName: 'test',
    http: [             // Define HTTP route
        {
            method: 'get',  // Enable a GET endpoint
            path: '/:uploadDate',  // this will be at route "/gridfs/myFiles/:md5"
            lookup: function (params, query) {  // uses express style url params
                return {filename: params.uploadDate};       // a query mapping url to myFiles
            }
        }]
});

Meteor.startup(function () {
    Meteor.publish('allData', function (clientUserId) {
        if (this.userId === clientUserId) {
            return myData.find({
                'metadata._Resumable': {
                    $exists: false
                },
                'metadata._auth.owner': this.userId
            });
        } else {
            return [];
        }
    });

    Meteor.publish('properties', function propertiesPublication() {
        return Properties.find();
    });

    Meteor.publish('floors', function floorsPublication() {
        return Floors.find();
    });

    Meteor.users.deny({
        update: function () {
            return true;
        }
    });
    return myData.allow({
        insert: function (userId, file) {
            var ref;
            file.metadata = (ref = file.metadata) != null ? ref : {};
            file.metadata._auth = {
                owner: userId
            };
            return true;
        },
        remove: function (userId, file) {
            var ref, ref1;
            if (((ref = file.metadata) != null ? (ref1 = ref._auth) != null ? ref1.owner : void 0 : void 0) && userId !== file.metadata._auth.owner) {
                return false;
            }
            return true;
        },
        read: function (userId, file) {
            var ref, ref1;
            if (((ref = file.metadata) != null ? (ref1 = ref._auth) != null ? ref1.owner : void 0 : void 0) && userId !== file.metadata._auth.owner) {
                return false;
            }
            return true;
        },
        write: function (userId, file, fields) {
            var ref, ref1;
            if (((ref = file.metadata) != null ? (ref1 = ref._auth) != null ? ref1.owner : void 0 : void 0) && userId !== file.metadata._auth.owner) {
                return false;
            }
            return true;
        }
    });
});


